import os
import pytest
import time
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture(scope='module')
def sharelatex_index(host):
    """Fixture that returns the sharelatex index

    Wait at least 60 seconds for the service to be up.
    Depending on the hardware we run the tests on, the startup
    can take quite some time.
    """
    for i in range(60):
        cmd = host.run('curl -H Host:sharelatex.docker.localhost -k -L https://localhost')
        if '502 Bad Gateway' not in cmd.stdout:
            break
        time.sleep(1)
    return cmd.stdout


def test_sharelatex_containers(host):
    with host.sudo():
        cmd = host.run('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.split('\n')[1:]])
    assert names == ['sharelatex', 'sharelatex_mongo', 'sharelatex_redis', 'traefik_proxy']


def test_sharelatex_index(sharelatex_index):
    # This tests that traefik forwards traffic to sharelatex
    # and that we can access the sharelatex index page
    assert '<title>Login - ESS ShareLaTeX, Online LaTeX Editor</title>' in sharelatex_index
